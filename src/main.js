import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// import Lang from 'vuejs-localization'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/scss/main.scss'
//var Lang = require('vuejs-localization');
import Lang from 'vuejs-localization'
import store from './store'

Lang.requireAll(require.context('./lang', true, /\.js$/));

Vue.config.productionTip = false
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(Lang)


new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
